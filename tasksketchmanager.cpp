#include "tasksketchmanager.h"

TaskSketchManager::TaskSketchManager() :
	QObject(nullptr),
	PluginBase(this),
	m_userTask(),
	m_dataExtention(new DataExtention(this, m_userTask))
{
	initPluginBase({
		{INTERFACE(IPlugin), this},
		{INTERFACE(IUserTaskNotesDataExtention), m_dataExtention},
		{INTERFACE(IDataExtention), m_dataExtention},
	},
	{
		{INTERFACE(IUserTaskDataExtention), m_userTask}
	});
}

TaskSketchManager::~TaskSketchManager()
{
}

void TaskSketchManager::onReferencesSet()
{
}

void TaskSketchManager::onReady()
{
}
