#pragma once

#include <QObject>
#include <QDebug>
#include <QString>
#include <QAbstractItemModel>

#include "../../Interfaces/Middleware/idataextention.h"
#include "../../Interfaces/Utility/i_user_task_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_notes_data_ext.h"
#include "../../Interfaces/Middleware/DataExtentionBase/DataExtentionBase.h"
#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"

class DataExtention : public QObject, public IUserTaskNotesDataExtention
{
	Q_OBJECT
	Q_INTERFACES(IUserTaskNotesDataExtention IDataExtention)
	DATA_EXTENTION_BASE_DEFINITIONS(IUserTaskNotesDataExtention, IUserTaskDataExtention, {"notes"})

public:
	DataExtention(QObject* parent, ReferenceInstancePtr<IUserTaskDataExtention> extention) :
		QObject(parent),
		m_extention(extention)
	{
	}

	virtual ~DataExtention() = default;

signals:
	void modelChanged() override;

private:
	// IUserTaskDataExtention interface
	QString notes() override
	{
		return QString();
	}

	QString name() override
	{
		return m_extention->name();
	}

	bool isDone() override
	{
		return m_extention->isDone();
	}

private:
	ReferenceInstancePtr<IUserTaskDataExtention> m_extention;
};
