#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"

#include "../../Interfaces/Utility/i_user_task_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_notes_data_ext.h"

#include "DataExtention.h"

class TaskSketchManager : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "PLAG.Plugin" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	TaskSketchManager();
	virtual ~TaskSketchManager() override;

public:
	// PluginBase interface
	virtual void onReferencesSet() override;
	virtual void onReady() override;

private:
	ReferenceInstancePtr<IUserTaskDataExtention> m_userTask;
	DataExtention* m_dataExtention;
};
